const express = require('express');
const app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());
app.get('/', require('./routes').index);
app.listen('3000', () => {
    console.log('We are live!');
});

